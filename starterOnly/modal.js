function editNav() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

// DOM Elements
const modalbg = document.querySelector(".bground");
const modalBtn = document.querySelectorAll(".modal-btn");
const formData = document.querySelectorAll(".formData"); 
const fermeture = document.querySelectorAll(".close");

const formulaire = document.getElementById('reserve'); // recuperation formulaire

const firstName = document.getElementById('first');

const lastName = document.getElementById('last');

const emailAddress = document.getElementById('email');

const controlDate= document.querySelector('input[type="date"]');


const inputNumber= document.getElementById('quantity');


const inputLocation= document.getElementsByName("location");
const elemLocation = document.getElementById('location1');

const cocher= document.getElementById('checkbox1');




let monregex = /^[a-zA-Z-\s]{2,}$/;
let emailRegex =/^[a-zA-Z0-9-._]+[@]{1}[a-zA-Z0-9-._]+[.]{1}[a-z]{2,10}$/; 
let numberRegex = /^[0-9]+$/; 

// launch modal event
modalBtn.forEach((btn) => btn.addEventListener("click", launchModal));

formulaire.addEventListener('submit', function(e) { 
  e.preventDefault();    
  if(validateForm()) {
    formulaire.innerHTML =" Merci ! Votre réservation a été reçue.";    
  }  
});

  
  
  
// evenment femrture lors du clic
fermeture.forEach(elem => elem.addEventListener("click", fermerModal)) 
// launch modal form
function launchModal() {
  modalbg.style.display = "block";
}
// fermeture de la modale
function fermerModal() {
  modalbg.style.display= "none";
}


 // fonction de validation de formulaire
const validateForm = () => {
  let valid= true;
 if(firstName.value.trim().length < 2 ) {
    verificationSaisi(firstName, "Le champs doit contenir plus de 2 caracteres");
    valid=false;
  } else if(monregex.test(firstName.value) == false){
    verificationSaisi(firstName, "le champs doit contenir au moins 2 lettres pas de chiffres");
    valid=false;
  } else {
    verificationSaisi(firstName, "");
  }


  if(lastName.value.trim().length < 2 ) {
     verificationSaisi(lastName, "Le champs doit contenir plus de 2 caracteres");
    valid=false;
  } else if(monregex.test(lastName.value) == false){
    verificationSaisi(lastName, 'le champs doit contenir au moins 2 lettres pas de chiffres');
    valid=false;
  } else {
    verificationSaisi(lastName, "");
  }

  if(emailAddress.value.trim()=="") {
    verificationSaisi(emailAddress, "Le champs est requis");
   valid=false;
 } else if(emailRegex.test(emailAddress.value) == false){
   verificationSaisi(emailAddress, "Entrez une adresse mail valide en incluant le symbole @ , pas d'espaces");
   valid=false;
 } else {
   verificationSaisi(emailAddress, "");
 }
 
 if(controlDate.value=="") {
  controlDate.max ="2004-01-01";
  verificationSaisi(controlDate, "Selectionnez une date valide");
   valid=false;
  } else {    
    verificationSaisi(controlDate, ' ');  
  } 

  if(inputNumber.value=="") {
   verificationSaisi(inputNumber, "Le champs est requis");
    valid=false;
  } else if(numberRegex.test(inputNumber.value) == false){
   verificationSaisi(inputNumber, 'le champs  ne doit contenaire que des chiffres');
    valid=false;
  } else {
   verificationSaisi(inputNumber, "");
  }

  if(!(inputLocation[0].checked || inputLocation[1].checked || inputLocation[2].checked || inputLocation[3].checked || inputLocation[4].checked || inputLocation[5].checked)) {
   verificationSaisi(elemLocation, 'Selectionnez une ville');
    valid=false;
   } else {
    verificationSaisi(elemLocation, "");
   }

   if(!cocher.checked) {
    verificationSaisi(cocher, "Cocher la case conditions générales");
     valid=false;
   } else {
    verificationSaisi(cocher, "");
   }

  return valid;
}

//fonction de verification de la saisi
const verificationSaisi = (champ, message) => {
  const parentChamp = champ.parentElement;
  const elemSpan = parentChamp.querySelector('div[id="msg"]');
  elemSpan.innerHTML=message;
  elemSpan.style.color= 'red';
  elemSpan.style.fontSize= '1rem';    
}

/*function validateDate() {
controlDate.min =2015-01-01;

}*/



 
 
 
